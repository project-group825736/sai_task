<?php

use App\Http\Controllers\DashboardContoller;
use App\Http\Controllers\UserContoller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::group(['middleware' => 'prevent-back-history'],function(){
Route::group(['middleware'=>'web'],function () {
    Route::get('/dashboard',[DashboardContoller::class,'index']);
    Route::get('/',[UserContoller::class,'userRegister']);
    Route::post('/register-user',[UserContoller::class,'addUser']);
    Route::get('/login',[UserContoller::class,'Login']);
    Route::post('/login',[UserContoller::class,'userLogin']);
    Route::get('/logout',[UserContoller::class,'userLogout']);
});  
});  



