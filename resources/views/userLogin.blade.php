@extends('layouts.main')
@section('main-container')

<section class="vh-100">
  <div class="container-fluid h-custom">

    
   
        <div class="card-form">
        <div class="text-center">
        <h2>{{$title}}</h2>
    </div>
            <form action="{{url($url)}}" method="post">
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
                @endif
                @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{Session::get('fail')}}
                </div>
                @endif
                @csrf
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Enter email">
                    <span class="text-danger">@error('email') {{$message}}@enderror </span>
                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="text-danger">@error('password') {{$message}}@enderror </span>
                </div>

                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>

                <!-- Register buttons -->
                <div class="text-center">
                    <p>Not a member? <a href="/">Register</a></p>
                  

                </div>
            </form>
        </div>
    
</div>
</section>
@endsection