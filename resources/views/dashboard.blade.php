@extends('layouts.main')
@section('main-container')
<section>
  <div class="container">

    <h1 class="text-center">User Profile</h1>
    <a href="/logout" class="btn btn-info float-right">Logout</a>
    <div class="card-columns ">
      <div class="card">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">User Profile</th>

            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Name</th>
              <td>{{$res['name']}}</td>
            </tr>
            <tr>
              <th scope="row">Email</th>
              <td>{{$res['email']}}</td>
            </tr>
            <tr>
              <th scope="row">Mobile</th>
              <td>{{$res['mobile']}}</td>
            </tr>
            <tr>
              <th scope="row">Date Of Birth</th>
              <td>{{date('d-m-Y', strtotime($res['dob']))}}</td>
            </tr>
            <tr>
              <th scope="row">Gender</th>
              <td>{{$res['gender']}}</td>
            </tr>
            <tr>
            <th scope="row">Address</th>
            <td>{{$res['address']}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @endsection
