@extends('layouts.main')
@section('main-container')
<section class="vh-100">
    <div class="container-fluid h-custom">
        <div class="card-form">

            <div class="text-center">
                <h2>{{$title}}</h2>
            </div>


            <form action="{{url($url)}}" method="post" enctype="multipart/form-data">
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
                @endif
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="">
                            <span class="text-danger">@error('name') {{$message}}@enderror </span>
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="">
                            <span class="text-danger">@error('email') {{$message}}@enderror </span>
                        </div>


                        <div class="form-group">
                            <label for="">Mobile</label>
                            <input type="number" name="mobile" value="{{ old('mobile') }}" class="form-control" placeholder="">
                            <span class="text-danger">@error('mobile') {{$message}}@enderror </span>
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>

                            <textarea name="address" id="" cols="35" rows="2">{{ old('address') }}</textarea>
                            <span class="text-danger" name="address">@error('address') {{$message}}@enderror </span>
                        </div>
                    </div>
                    <div class="col-md-6">


                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" value="{{@$data['password']}}" class="form-control" placeholder="">
                            <span class="text-danger">@error('password') {{$message}}@enderror </span>
                        </div>
                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input type="password" name="confirm_password" value="{{@$data['password']}}" class="form-control" placeholder="">
                            <span class="text-danger">@error('confirm_password') {{$message}}@enderror </span>
                        </div>
                        <div class="form-group">
                            <label for="">Date</label>
                            <input type="date" name="date_of_birth" value="{{ old('date_of_birth') }}" class="form-control" placeholder="">
                            <span class="text-danger">@error('date_of_birth') {{$message}}@enderror </span>
                        </div>

                        <div class="form-group">

                            <div class="form-gender">
                                <label for="">Gender</label>
                                <div class="row">

                                    <div class="col-md-4 gender-row">
                                        Male <input type="radio" class="form-check-input" name="gender" value="male">
                                    </div>
                                    <div class="col-md-4 gender-row"> Female <input type="radio" class="form-check-input" name="gender" value="female">
                                       
                                    </div>
                                </div>
                                <span class="text-danger">@error('gender') {{$message}}@enderror </span>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block mb-4">Sign Up</button>

                    <!-- Register buttons -->
                    <div class="text-login">
                        <p>Already a member? <a href="{{url('login')}}">Login</a></p>
                     </div>

            </form>
        </div>


    </div>
</section>
@endsection