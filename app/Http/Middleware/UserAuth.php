<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
       
        $path = $request->path();
        if(($path == 'login' || $path == '/') && session()->get('login')){
            return redirect()->to('dashboard');
        }else if( $path == 'dashboard' && !session()->get('login')) {
            return redirect()->to('login');
        }

        return $next($request);
    }
}
