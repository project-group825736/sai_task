<?php

namespace App\Http\Controllers;
use App\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class UserContoller extends Controller
{
   
   public function userRegister(){
    $title = 'User Register';
    $url = 'register-user';
    return view('userRegister',compact('title',"url"));
   }

   public function Login(){
    $title = 'User Login';
    $url = 'login';
    return view('userLogin',compact('title','url'));
   }

   public function userLogin(request $request)
   {
     $request->validate([
        'email'=>'required|email:rfc,dns',
        'password'=>'required',
        ]);
        $model = new UserModel();
        $res = $model::where('email','=',$request->email)->first();
      
        if($res){
            if(Hash::check($request->password,$res ->password)){
                $request->session()->put('login',$res->id);
                return redirect()->to('dashboard');

            }else{
                return back()->withFail( 'Your Username or Password Wrong'); 
            }
        }else{
            return back()->withFail( 'This Username is not registered ');
        }
      
   }

   public function addUser(request $request)
    {
        $request->validate([
            'name'=>'required|regex:/^[\pL\s]+$/u|min:3',
            'email'=>'required|email:rfc,dns|unique:user',
            'mobile'=>'digits:10|numeric',
            'password'=>'required|min:8',
            'confirm_password'=>'required|same:password',
            'date_of_birth'=>'required|date',
            'gender'=>'required',
           
        ]);
        $model = new UserModel();
        $model->name = $request->name;
        $model->email  = $request->email;
        $model->mobile = $request->mobile;
        $model->password = Hash::make($request->password);
        $model->dob = $request->date_of_birth;
        $model->address = $request->address;
        $model->gender = $request->gender;
        $res =  $model->save();
        if($res){
            return redirect()->to('login')->withSuccess('You have registerd successfully');
        }else{
            return back()->withFail( 'You have not registerd');
        }
    }

        public function userLogout(){
        session()->forget('login');
        return redirect()->to('login');
       
       }


}
